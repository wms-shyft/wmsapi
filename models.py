# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class TAreaMaster(models.Model):
    area_id = models.CharField(primary_key=True, max_length=20)
    org_id = models.CharField(max_length=20, blank=True, null=True)
    area_name = models.CharField(max_length=100, blank=True, null=True)
    area_type = models.CharField(max_length=100, blank=True, null=True)
    warehouse_id = models.CharField(max_length=20, blank=True, null=True)
    is_active = models.BooleanField()
    created_by = models.CharField(max_length=20, blank=True, null=True)
    created_on = models.DateTimeField()
    updated_by = models.CharField(max_length=20, blank=True, null=True)
    updated_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_area_master'


class TCityMaster(models.Model):
    city_id = models.CharField(primary_key=True, max_length=20)
    org_id = models.CharField(max_length=20, blank=True, null=True)
    city_name = models.CharField(max_length=100, blank=True, null=True)
    country_id = models.CharField(max_length=20, blank=True, null=True)
    is_default = models.BooleanField()
    created_by = models.CharField(max_length=20, blank=True, null=True)
    created_on = models.DateTimeField()
    updated_by = models.CharField(max_length=20, blank=True, null=True)
    updated_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_city_master'


class TCountryMaster(models.Model):
    country_id = models.CharField(primary_key=True, max_length=20)
    org_id = models.CharField(max_length=20, blank=True, null=True)
    country_name = models.CharField(max_length=100, blank=True, null=True)
    two_char_code = models.CharField(max_length=2, blank=True, null=True)
    three_char_code = models.CharField(max_length=3, blank=True, null=True)
    currency_code = models.CharField(max_length=10, blank=True, null=True)
    currency_name = models.CharField(max_length=100, blank=True, null=True)
    is_active = models.BooleanField()
    created_by = models.CharField(max_length=20, blank=True, null=True)
    created_on = models.DateTimeField()
    updated_by = models.CharField(max_length=20, blank=True, null=True)
    updated_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_country_master'


class TLocationMaster(models.Model):
    location_id = models.CharField(primary_key=True, max_length=20)
    org_id = models.CharField(max_length=20, blank=True, null=True)
    area_id = models.CharField(max_length=20, blank=True, null=True)
    warehouse_id = models.CharField(max_length=20, blank=True, null=True)
    shelve = models.CharField(max_length=100, blank=True, null=True)
    row = models.CharField(max_length=100, blank=True, null=True)
    column = models.CharField(max_length=100, blank=True, null=True)
    is_active = models.BooleanField()
    created_by = models.CharField(max_length=20, blank=True, null=True)
    created_on = models.DateTimeField()
    updated_by = models.CharField(max_length=20, blank=True, null=True)
    updated_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_location_master'


class TRoleMaster(models.Model):
    role_id = models.CharField(primary_key=True, max_length=20)
    org_id = models.CharField(max_length=20, blank=True, null=True)
    role_name = models.CharField(max_length=100)
    is_active = models.BooleanField()
    created_by = models.CharField(max_length=20, blank=True, null=True)
    created_on = models.DateTimeField()
    update_by = models.CharField(max_length=20, blank=True, null=True)
    update_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_role_master'


class TUserMaster(models.Model):
    user_id = models.CharField(primary_key=True, max_length=20)
    org_id = models.CharField(max_length=20, blank=True, null=True)
    role_id = models.CharField(max_length=20)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    country_code = models.CharField(max_length=100, blank=True, null=True)
    mobile = models.CharField(max_length=100, blank=True, null=True)
    password = models.CharField(max_length=200, blank=True, null=True)
    is_active = models.BooleanField()
    created_by = models.CharField(max_length=100, blank=True, null=True)
    created_on = models.DateTimeField()
    updated_by = models.CharField(max_length=100, blank=True, null=True)
    updated_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_user_master'


class TWarehouseMaster(models.Model):
    warehouse_id = models.CharField(primary_key=True, max_length=20)
    org_id = models.CharField(max_length=20, blank=True, null=True)
    warehouse_name = models.CharField(max_length=100, blank=True, null=True)
    city_id = models.CharField(max_length=20, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    zipcode = models.CharField(max_length=10, blank=True, null=True)
    inchareged_user_id = models.CharField(max_length=20, blank=True, null=True)
    is_default = models.BooleanField()
    is_active = models.BooleanField()
    created_by = models.CharField(max_length=20, blank=True, null=True)
    created_on = models.DateTimeField()
    updated_by = models.CharField(max_length=20, blank=True, null=True)
    updated_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_warehouse_master'
