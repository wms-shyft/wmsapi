from datetime import datetime

from django.db import connection

def getuniqueid():
    d=datetime.today().strftime('%y%m%d%H%M%S%f%f')
    return str(d[:-5])

def getlateststatus():
        sql = ('select o.task_id,s.status,l.location_name from public.t_orderdetails o '
                'join t_orderstatus s on o.status=s.status_id '
                'join t_location_master l on l.location_id=o.location_id')
        cursor = connection.cursor()
      
        cursor.execute(sql)
        data = cursor.fetchall()       
        return data

def getlateststatusbyid(taskid):
        sql = ('select s.status,l.location_name from public.t_orderdetails o '
             'join t_orderstatus s on o.status=s.status_id  '
             'join t_location_master l on l.location_id=o.location_id '
             'where task_id='"'"+taskid+"'"'')
        cursor = connection.cursor()
      
        cursor.execute(sql)
        # desc = cursor.description
        data = cursor.fetchone()        
        if(data == None):
            return None
        else:
            return data
        # row= [dict(zip([col[0] for col in desc], row))
        #       for row in cursor.fetchall()]
        # rowe=row
        # (k, v), =rowe[0].items()
        # print(v)

        