from django.db import models

class AreaTypeMaster(models.Model):
    areatype_id = models.CharField(primary_key=True, max_length=20)
    org_id = models.CharField(max_length=20, blank=True, null=True)
    areatype = models.CharField(max_length=100, blank=True, null=True)
    areatype_code = models.CharField(max_length=100, blank=True, null=True)    
    is_active = models.BooleanField()
    created_by = models.CharField(max_length=20, blank=True, null=True)
    created_on = models.DateTimeField()
    updated_by = models.CharField(max_length=20, blank=True, null=True)
    updated_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_areatype_master'
