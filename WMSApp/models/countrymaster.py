from django.db import models

class TCountryMaster(models.Model):
    country_id = models.CharField(primary_key=True, max_length=20)
    org_id = models.CharField(max_length=20, blank=True, null=True)
    country_name = models.CharField(max_length=100, blank=True, null=True)
    two_char_code = models.CharField(max_length=2, blank=True, null=True)
    three_char_code = models.CharField(max_length=3, blank=True, null=True)
    currency_code = models.CharField(max_length=10, blank=True, null=True)
    currency_name = models.CharField(max_length=100, blank=True, null=True)
    is_active = models.BooleanField()
    created_by = models.CharField(max_length=20, blank=True, null=True)
    created_on = models.DateTimeField()
    updated_by = models.CharField(max_length=20, blank=True, null=True)
    updated_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_country_master'
