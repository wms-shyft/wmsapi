from django.db import models

class LocationMaster(models.Model):
    location_id = models.CharField(primary_key=True, max_length=20)
    org_id = models.CharField(max_length=20, blank=True, null=True)
    location_name=models.CharField(max_length=100,blank=True,null=True)
    areatype_code = models.CharField(max_length=100, blank=True, null=True)
    warehouse_code = models.CharField(max_length=100, blank=True, null=True)
    shelve = models.CharField(max_length=100, blank=True, null=True)
    row = models.CharField(max_length=100, blank=True, null=True)
    column = models.CharField(max_length=100, blank=True, null=True)
    is_active = models.BooleanField()
    created_by = models.CharField(max_length=20, blank=True, null=True)
    created_on = models.DateTimeField()
    updated_by = models.CharField(max_length=20, blank=True, null=True)
    updated_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_location_master'
