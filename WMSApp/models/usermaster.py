from django.db import models

class UserMaster(models.Model):
    user_id = models.CharField(primary_key=True, max_length=20)
    org_id = models.CharField(max_length=20, blank=True, null=True)
    role_id = models.CharField(max_length=20)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    country_code = models.CharField(max_length=100, blank=True, null=True)
    mobile = models.CharField(max_length=100, blank=True, null=True)
    password = models.CharField(max_length=200, blank=True, null=True)
    is_active = models.BooleanField()
    created_by = models.CharField(max_length=100, blank=True, null=True)
    created_on = models.DateTimeField()
    updated_by = models.CharField(max_length=100, blank=True, null=True)
    updated_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_user_master'
