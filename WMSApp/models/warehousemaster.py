from django.db import models

class WarehouseMaster(models.Model):
    warehouse_id = models.CharField(primary_key=True, max_length=20)
    org_id = models.CharField(max_length=20, blank=True, null=True)
    warehouse_name = models.CharField(max_length=100, blank=True, null=True)
    warehouse_code = models.CharField(max_length=100, blank=True, null=True)
    city_id = models.CharField(max_length=20, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    zipcode = models.CharField(max_length=10, blank=True, null=True)
    inchareged_user_id = models.CharField(max_length=20, blank=True, null=True)
    is_default = models.BooleanField()
    is_active = models.BooleanField()
    created_by = models.CharField(max_length=20, blank=True, null=True)
    created_on = models.DateTimeField()
    updated_by = models.CharField(max_length=20, blank=True, null=True)
    updated_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_warehouse_master'
