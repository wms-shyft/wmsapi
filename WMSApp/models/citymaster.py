from django.db import models

class CityMaster(models.Model):
    city_id = models.CharField(primary_key=True, max_length=20)
    org_id = models.CharField(max_length=20, blank=True, null=True)
    city_name = models.CharField(max_length=100, blank=True, null=True)
    country_id = models.CharField(max_length=20, blank=True, null=True)
    is_default = models.BooleanField()
    created_by = models.CharField(max_length=20, blank=True, null=True)
    created_on = models.DateTimeField()
    updated_by = models.CharField(max_length=20, blank=True, null=True)
    updated_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_city_master'
