from django.urls.conf import re_path
from WMSApp.views.areamaster import AreaMasterViews
from WMSApp.views.areatypemaster import AreaTypeMasterViews
from WMSApp.views.citymaster import CityMasterViews
from WMSApp.views.getdata import  GetAreaDetailsView, GetLocationDetailsView, GetLocationView, GetOrderHistoryView, GetStatusView, GetWareHouseView, GetWarehouseDetailsView,GetAreaTypeView
from WMSApp.views.locationmaster import LocationMasterViews
from WMSApp.views.orderdetails import OrderDetailsViews, SaveUpdateStatus
from WMSApp.views.orderstatusmaster import OrderStatusMasterViews
from WMSApp.views.pickuplistdetails import GetFeetDetailsView, PickUpListDetailsViews
from WMSApp.views.usermaster import UserMasterViews
from WMSApp.views.warehousemaster import  WareHouseMasterViews

urlpatterns = [
    re_path(r'^warehouse$', WareHouseMasterViews.as_view()),
    re_path(r'^warehouse/([0-9]+)$', WareHouseMasterViews.as_view()),
    re_path(r'^user$', UserMasterViews.as_view()),
    re_path(r'^user/([0-9]+)$', UserMasterViews.as_view()),
    re_path(r'^location$', LocationMasterViews.as_view()),
    re_path(r'^location/([0-9]+)$', LocationMasterViews.as_view()),
    re_path(r'^orderstatus$', OrderStatusMasterViews.as_view()),
    re_path(r'^orderstatus/([0-9]+)$', OrderStatusMasterViews.as_view()),
    re_path(r'^city$', CityMasterViews.as_view()),
    re_path(r'^area$', AreaMasterViews.as_view()),
    re_path(r'^area/([0-9]+)$', AreaMasterViews.as_view()),
    re_path(r'^orderdetails$', OrderDetailsViews.as_view()),
    re_path(r'^orderdetails/(.*)', OrderDetailsViews.as_view()),
    re_path(r'^savestatus$', SaveUpdateStatus.as_view()),
    re_path(r'^savestatus/(.*)', SaveUpdateStatus.as_view()),
    re_path(r'^pickupdetails$', PickUpListDetailsViews.as_view()),
    re_path(r'^pickupdetails/(.*)', PickUpListDetailsViews.as_view()),
    re_path(r'^getfleet/(.*)', GetFeetDetailsView.as_view()),
    re_path(r'^areatype$', AreaTypeMasterViews.as_view()),
    re_path(r'^areatype/([0-9]+)$', AreaTypeMasterViews.as_view()),
    re_path(r'^getwarehousedetails$', GetWarehouseDetailsView.as_view()),
    re_path(r'^getareadetails$', GetAreaDetailsView.as_view()),
    re_path(r'^getlocationdetails$', GetLocationDetailsView.as_view()),
    re_path(r'^getareatype/([A-Za-z]+)$', GetAreaTypeView.as_view()),
    re_path(r'^getstatus/([A-Za-z]+)$', GetStatusView.as_view()),
    re_path(r'^getlocation/([A-Za-z]+)$', GetLocationView.as_view()),
    re_path(r'^getwarehouse/([A-Za-z]+)$', GetWareHouseView.as_view()),
    re_path(r'^getorderhistory/(.*)', GetOrderHistoryView.as_view())

]
