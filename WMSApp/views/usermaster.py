from datetime import datetime
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from WMSApp import util
from WMSApp.models.usermaster import UserMaster
from WMSApp.serializers.usermaster import  UserSerializer



class UserMasterViews(APIView):

    def get(self, request, id=None):
        if id:
            item = UserMaster.objects.get(user_id=id)
            serializer = UserSerializer(item)
            return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)

        items = UserMaster.objects.all()
        serializer = UserSerializer(items, many=True)
        return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)

    def post(self, request):
        request.data["user_id"]=util.getuniqueid()
        request.data["created_on"]=datetime.utcnow()        
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)
        else:
            return Response({"status": "error", "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, id=None):
        item = UserMaster.objects.get(user_id=id)
        request.data["updated_on"]=datetime.utcnow()        
        serializer = UserSerializer(item, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)
        else:
            return Response({"status": "error", "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id=None):
        item = get_object_or_404(UserMaster, user_id=id)
        item.delete()
        return Response({"status": "success", "data": "Item Deleted"})
