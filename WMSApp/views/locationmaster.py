from datetime import datetime
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from WMSApp import util
from WMSApp.models.locationmaster import LocationMaster
from WMSApp.serializers.locationmaster import  LocationSerializer



class LocationMasterViews(APIView):

    def get(self, request, id=None):
        if id:
            item = LocationMaster.objects.get(location_id=id)
            serializer = LocationSerializer(item)
            return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)

        items = LocationMaster.objects.all()
        serializer = LocationSerializer(items, many=True)
        return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)

    def post(self, request):
        request.data["location_id"] = util.getuniqueid()
        request.data["location_name"]=request.data["shelve"]+ request.data["row"]+ request.data["column"] + "-" + request.data["areatype_code"] + "-" + request.data["warehouse_code"]
        request.data["created_on"]=datetime.utcnow()
        serializer = LocationSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)
        else:
            return Response({"status": "error", "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, id=None):
        item = LocationMaster.objects.get(location_id=id)
        request.data["updated_on"]=datetime.utcnow()
        serializer = LocationSerializer(item, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)
        else:
            return Response({"status": "error", "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id=None):
        item = get_object_or_404(LocationMaster, location_id=id)
        item.delete()
        return Response({"status": "success", "data": "Item Deleted"})
