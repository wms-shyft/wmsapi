from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from WMSApp.models.orderstatusmaster import OrderStatusMaster
from WMSApp.serializers.orderstatusmaster import  OrderStatusMasterSerializer



class OrderStatusMasterViews(APIView):

    def get(self, request, id=None):
        if id:
            item = OrderStatusMaster.objects.get(status_id=id)
            serializer = OrderStatusMasterSerializer(item)
            return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)

        items = OrderStatusMaster.objects.all()
        serializer = OrderStatusMasterSerializer(items, many=True)
        return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)

    