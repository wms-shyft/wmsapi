from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from WMSApp import util
from WMSApp.models.areatypemaster import AreaTypeMaster
from WMSApp.serializers.areatypemaster import  AreaTypeMasterSerializer



class AreaTypeMasterViews(APIView):

    def get(self, request, id=None):
        if id:
            item = AreaTypeMaster.objects.get(location_id=id)
            serializer = AreaTypeMasterSerializer(item)
            return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)

        items = AreaTypeMaster.objects.all()
        serializer = AreaTypeMasterSerializer(items, many=True)
        return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)

