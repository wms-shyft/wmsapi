from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from WMSApp.models.citymaster import CityMaster
from WMSApp.serializers.citymaster import  CitySerializer


class CityMasterViews(APIView):

    def get(self, request, id=None):
        if id:
            item = CityMaster.objects.get(location_id=id)
            serializer = CitySerializer(item)
            return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)

        items = CityMaster.objects.all()
        serializer = CitySerializer(items, many=True)
        return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)
