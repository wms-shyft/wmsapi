from django.shortcuts import get_object_or_404
from django.db import connection
from django.http.response import JsonResponse
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.views import APIView
from django.forms.models import model_to_dict

class GetWarehouseDetailsView(APIView):

    def get(self, request):       
        sql = '''select w.*,concat(u.first_name ,' ', u.last_name ) as person_name,u.mobile as person_contact,c.city_name from public.t_warehouse_master w
                join public.t_user_master u on w.inchareged_user_id=u.user_id
                join public.t_city_master c on w.city_id=c.city_id'''
        cursor = connection.cursor()
      
        cursor.execute(sql)
        # row1=self.dictfetchall(cursor)
        desc = cursor.description
        row= [dict(zip([col[0] for col in desc], row))
              for row in cursor.fetchall()]
       
        return JsonResponse(row,safe=False, status=status.HTTP_200_OK)
        
class GetAreaDetailsView(APIView):   
    def get(self, request):       
        sql = '''select ar.*,w.warehouse_name,ae.areatype from public.t_area_master ar
                join public.t_warehouse_master w on w.warehouse_code=ar.warehouse_code
                join public.t_areatype_master ae on ar.areatype_code=ae.areatype_code'''
        cursor = connection.cursor()
      
        cursor.execute(sql)
        # row1=self.dictfetchall(cursor)
        desc = cursor.description
        row= [dict(zip([col[0] for col in desc], row))
              for row in cursor.fetchall()]
       
        return JsonResponse(row,safe=False, status=status.HTTP_200_OK)
        
class GetLocationDetailsView(APIView):   
    def get(self, request):       
        sql = '''select l.*,w.warehouse_name,ae.areatype from public.t_location_master l
                join public.t_warehouse_master w on w.warehouse_code=l.warehouse_code
                join public.t_areatype_master ae on l.areatype_code=ae.areatype_code'''
        cursor = connection.cursor()
      
        cursor.execute(sql)
        # row1=self.dictfetchall(cursor)
        desc = cursor.description
        row= [dict(zip([col[0] for col in desc], row))
              for row in cursor.fetchall()]
       
        return JsonResponse(row,safe=False, status=status.HTTP_200_OK)

class GetAreaTypeView(APIView):   
    def get(self, request,id=None):       
        sql = "select concat(ar.areatype,'-' ,ar.areatype_code) as name, ar.areatype_code as value from t_areatype_master ar where ar.areatype ilike '%"+id+"%' or ar.areatype_code ilike '%"+id+"%'"
        cursor = connection.cursor()
      
        cursor.execute(sql)
        # row1=self.dictfetchall(cursor)
        desc = cursor.description
        row= [dict(zip([col[0] for col in desc], row))
              for row in cursor.fetchall()]
       
        return JsonResponse(row,safe=False, status=status.HTTP_200_OK)
             
class GetWareHouseView(APIView):   
    def get(self, request,id=None):       
        sql = "select concat(wr.warehouse_name,'-' ,wr.warehouse_code) as name, wr.warehouse_code as value from t_warehouse_master wr where wr.warehouse_name ilike '%"+id+"%' or wr.warehouse_code ilike '%"+id+"%'"
        cursor = connection.cursor()
      
        cursor.execute(sql)
        # row1=self.dictfetchall(cursor)
        desc = cursor.description
        row= [dict(zip([col[0] for col in desc], row))
              for row in cursor.fetchall()]
       
        return JsonResponse(row,safe=False, status=status.HTTP_200_OK)

class GetLocationView(APIView):   
    def get(self, request,id=None):       
        sql = "select location_name as name,location_id as value from t_location_master where location_name ilike '%"+id+"%'"
        cursor = connection.cursor()
      
        cursor.execute(sql)
        # row1=self.dictfetchall(cursor)
        desc = cursor.description
        row= [dict(zip([col[0] for col in desc], row))
              for row in cursor.fetchall()]
       
        return JsonResponse(row,safe=False, status=status.HTTP_200_OK)

class GetStatusView(APIView):   
    def get(self, request,id=None):       
        sql = "select status as name ,status_id as value from t_orderstatus where status ilike '%"+id+"%'"
        cursor = connection.cursor()
      
        cursor.execute(sql)
        desc = cursor.description
        row= [dict(zip([col[0] for col in desc], row))
              for row in cursor.fetchall()]
       
        return JsonResponse(row,safe=False, status=status.HTTP_200_OK)

class GetOrderHistoryView(APIView):   
    def get(self, request,id=None):       
        sql = '''select o.*,s.status,l.location_name,(u.first_name ||' '|| u.last_name) as  updatedby from public.t_orderdetails_logs o 
            join public.t_orderstatus s on o.status=s.status_id
            join public.t_location_master l on o.location_id=l.location_id
			join public.t_user_master u on o.created_by=u.user_id
            where o.task_id='strtaskid'
            order by o.task_id desc'''
        sql=sql.replace("strtaskid", id)
        cursor = connection.cursor()
      
        cursor.execute(sql)
        desc = cursor.description
        row= [dict(zip([col[0] for col in desc], row))
              for row in cursor.fetchall()]
       
        return JsonResponse(row,safe=False, status=status.HTTP_200_OK)

