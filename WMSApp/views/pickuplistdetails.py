from datetime import date, timedelta
from rest_framework import status
from rest_framework.views import APIView
from WMSApp import gethostname, util
import json
from rest_framework.response import Response
from django.http.response import JsonResponse
from django.db import connection


class PickUpListDetailsViews(APIView):

    def post(self, request, id=None):
        # startdate=date.today()-timedelta(7) if request.data["startdate"]==None else request.data["startdate"]
        # enddate=date.today()+timedelta(2) if request.data["enddate"]==None else request.data["enddate"]        
        scheduleddate= request.data["scheduleddate"]
        fleetname= request.data["fleetname"]
        
        if id:
            if scheduleddate!= None:
             sql = '''select go_tasks.booking_id as Task_ID,go_tasks.fleet_name, go_tasks.time as scheduled_date, go_tasks.fleet_phone ,go_tasks.name as Customer_Name,
                null as Order_Status,null as Location,go_tasks.job_address as Address, go_tasks.cust_collected_amt as COD
                from go_order left join go_tasks on go_order.order_id = go_tasks.fk_order_id
                where  go_tasks.job_type='Delivery' and go_order.currency ='AED'                 
                and Date(go_tasks.job_time_utc) = '{scheduleddate}'
                and fleet_name like '%{fleetname}%'
                and go_tasks.booking_id='strtaskid'
                order by go_tasks.task_id desc'''.format(scheduleddate=scheduleddate,fleetname=fleetname)  
            if scheduleddate==None:
             sql = '''select go_tasks.booking_id as Task_ID,go_tasks.fleet_name, go_tasks.time as scheduled_date, go_tasks.fleet_phone ,go_tasks.name as Customer_Name,
                null as Order_Status,null as Location,go_tasks.job_address as Address, go_tasks.cust_collected_amt as COD
                from go_order left join go_tasks on go_order.order_id = go_tasks.fk_order_id
                where  go_tasks.job_type='Delivery' and go_order.currency ='AED'  
                and fleet_name like '%{fleetname}%'
                and go_tasks.booking_id='strtaskid'
                order by go_tasks.task_id desc'''.format(fleetname=fleetname)     
            sql=sql.replace("strtaskid", id)
            df=gethostname.query(sql) 
            for i, row in df.iterrows():
                ifor_val=util.getlateststatusbyid(row[0])
                if(ifor_val != None):
                    df.at[i , 'Order_Status'] = ifor_val[0]
                    df.at[i , 'Location'] = ifor_val[1]
            listOfReading = [json.loads(row.to_json()) for index, row in df.iterrows()]
            
            return Response({"status": "success", "data":listOfReading}, status=status.HTTP_200_OK)
        
        if scheduleddate!= None:
            sql = '''select go_tasks.booking_id as Task_ID,go_tasks.fleet_name, go_tasks.time as scheduled_date, go_tasks.fleet_phone ,go_tasks.name as Customer_Name,
                null as Order_Status,null as Location,go_tasks.job_address as Address, go_tasks.cust_collected_amt as COD
                from go_order left join go_tasks on go_order.order_id = go_tasks.fk_order_id
                where  go_tasks.job_type='Delivery' and go_order.currency ='AED'                
                and Date(go_tasks.job_time_utc) = '{scheduleddate}'
                and fleet_name like '%{fleetname}%'
                order by go_tasks.task_id desc'''.format(scheduleddate=scheduleddate,fleetname=fleetname)        
        if scheduleddate == None:
            sql = '''select go_tasks.booking_id as Task_ID,go_tasks.fleet_name, go_tasks.time as scheduled_date, go_tasks.fleet_phone ,go_tasks.name as Customer_Name,
                null as Order_Status,null as Location,go_tasks.job_address as Address, go_tasks.cust_collected_amt as COD
                from go_order left join go_tasks on go_order.order_id = go_tasks.fk_order_id
                where  go_tasks.job_type='Delivery' and go_order.currency ='AED'
                and fleet_name like '%{fleetname}%'
                order by go_tasks.task_id desc
                '''.format(fleetname=fleetname)        
        df=gethostname.query(sql)   
        rowsdata = util.getlateststatus()      
        for i, row in df.iterrows():
            if(any(item in row[0] for item in [elem[0] for elem in rowsdata])):                
                df.at[i , 'Order_Status'] = [x[1] for x in rowsdata if x[0] == row[0]][0]
                df.at[i , 'Location'] = [x[2] for x in rowsdata if x[0] == row[0]][0]

        listOfReading = [json.loads(row.to_json()) for index, row in df.iterrows()]
            
        return Response({"status": "success", "data":listOfReading}, status=status.HTTP_200_OK)

class GetFeetDetailsView(APIView):   
    def get(self, request,id=None):       
        sql = "select rider_name as value,rider_name as name from go_rider_details where rider_name is not null and rider_name like '%"+id+"%' limit 10"
        df=gethostname.query(sql)   
        
        listOfData = [json.loads(row.to_json()) for index, row in df.iterrows()]
            
        return Response(listOfData, status=status.HTTP_200_OK)
