from datetime import datetime
from datetime import date, timedelta
from lib2to3.pgen2 import driver
from django.db import connection
from django.http.response import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from WMSApp.models.orderdetails import OrderDetails
from WMSApp.models.orderdetailslog import OrderDetailsLog
import json

from WMSApp import gethostname, util

class OrderDetailsViews(APIView):

    def post(self, request, id=None):
        #startdate=date.today()-timedelta(7) if request.data["startdate"]==None else request.data["startdate"]
        #enddate=date.today()+timedelta(2) if request.data["enddate"]==None else request.data["enddate"]        
        #scheduleddate=date.today() if request.data["scheduleddate"]==None else request.data["scheduleddate"]
        startdate=request.data["startdate"]
        enddate=request.data["enddate"]
        if id:
            if startdate != None and enddate != None:
             sql = '''select go_tasks.booking_id as Task_ID,go_tasks.created_date as Creation_Date,  go_tasks.task_status as Order_Status,go_tasks.job_time_utc as Scheduled_Date,go_tasks.no_of_items as No_of_Items, go_tasks.name as Customer_Name, go_tasks.job_address as Address, go_tasks.cust_collected_amt as COD
                ,null as Location from go_order left join go_tasks on go_order.order_id = go_tasks.fk_order_id
                where  go_tasks.job_type='Delivery' and go_order.currency ='AED' 
                and (Date(go_tasks.created_date) between '{startdate}' and '{enddate}')
                and go_tasks.booking_id='strtaskid'
                order by go_tasks.task_id desc'''.format(startdate=startdate,enddate=enddate)
            if startdate != None and enddate == None:
                sql = '''select go_tasks.booking_id as Task_ID,go_tasks.created_date as Creation_Date,  go_tasks.task_status as Order_Status,go_tasks.job_time_utc as Scheduled_Date,go_tasks.no_of_items as No_of_Items, go_tasks.name as Customer_Name, go_tasks.job_address as Address, go_tasks.cust_collected_amt as COD
                ,null as Location from go_order left join go_tasks on go_order.order_id = go_tasks.fk_order_id
                where  go_tasks.job_type='Delivery' and go_order.currency ='AED' 
                and Date(go_tasks.created_date) >= '{startdate}'
                and go_tasks.booking_id='strtaskid'
                order by go_tasks.task_id desc'''.format(startdate=startdate)
            if startdate == None and enddate != None:
                sql = '''select go_tasks.booking_id as Task_ID,go_tasks.created_date as Creation_Date,  go_tasks.task_status as Order_Status,go_tasks.job_time_utc as Scheduled_Date,go_tasks.no_of_items as No_of_Items, go_tasks.name as Customer_Name, go_tasks.job_address as Address, go_tasks.cust_collected_amt as COD
                ,null as Location from go_order left join go_tasks on go_order.order_id = go_tasks.fk_order_id
                where  go_tasks.job_type='Delivery' and go_order.currency ='AED' 
                and Date(go_tasks.created_date) <= '{enddate}'
                and go_tasks.booking_id='strtaskid'
                order by go_tasks.task_id desc'''.format(enddate=enddate)
            if startdate == None and enddate == None:
                sql = '''select go_tasks.booking_id as Task_ID,go_tasks.created_date as Creation_Date,  go_tasks.task_status as Order_Status,go_tasks.job_time_utc as Scheduled_Date,go_tasks.no_of_items as No_of_Items, go_tasks.name as Customer_Name, go_tasks.job_address as Address, go_tasks.cust_collected_amt as COD
                ,null as Location from go_order left join go_tasks on go_order.order_id = go_tasks.fk_order_id
                where  go_tasks.job_type='Delivery' and go_order.currency ='AED' 
                and go_tasks.booking_id='strtaskid'
                order by go_tasks.task_id desc'''
            sql=sql.replace("strtaskid", id)
            df=gethostname.query(sql)          
             
            for i, row in df.iterrows():
                ifor_val=util.getlateststatusbyid(row[0])
                if(ifor_val!=None):
                    df.at[i , 'Order_Status'] = ifor_val[0]
                    df.at[i , 'Location'] = ifor_val[1]
            listOfReading = [json.loads(row.to_json()) for index, row in df.iterrows()]
            
            return Response({"status": "success", "data":listOfReading}, status=status.HTTP_200_OK)
        
        if startdate != None and enddate != None:
             sql = '''select go_tasks.booking_id as Task_ID,go_tasks.created_date as Creation_Date,  go_tasks.task_status as Order_Status,go_tasks.job_time_utc as Scheduled_Date,go_tasks.no_of_items as No_of_Items, go_tasks.name as Customer_Name, go_tasks.job_address as Address, go_tasks.cust_collected_amt as COD
                ,null as Location from go_order left join go_tasks on go_order.order_id = go_tasks.fk_order_id
                where  go_tasks.job_type='Delivery' and go_order.currency ='AED' 
                and (Date(go_tasks.created_date) between '{startdate}' and '{enddate}')
                order by go_tasks.task_id desc'''.format(startdate=startdate,enddate=enddate)
        if startdate != None and enddate == None:
                sql = '''select go_tasks.booking_id as Task_ID,go_tasks.created_date as Creation_Date,  go_tasks.task_status as Order_Status,go_tasks.job_time_utc as Scheduled_Date,go_tasks.no_of_items as No_of_Items, go_tasks.name as Customer_Name, go_tasks.job_address as Address, go_tasks.cust_collected_amt as COD
                ,null as Location from go_order left join go_tasks on go_order.order_id = go_tasks.fk_order_id
                where  go_tasks.job_type='Delivery' and go_order.currency ='AED' 
                and Date(go_tasks.created_date) >= '{startdate}'
                order by go_tasks.task_id desc'''.format(startdate=startdate)
        if startdate == None and enddate != None:
                sql = '''select go_tasks.booking_id as Task_ID,go_tasks.created_date as Creation_Date,  go_tasks.task_status as Order_Status,go_tasks.job_time_utc as Scheduled_Date,go_tasks.no_of_items as No_of_Items, go_tasks.name as Customer_Name, go_tasks.job_address as Address, go_tasks.cust_collected_amt as COD
                ,null as Location from go_order left join go_tasks on go_order.order_id = go_tasks.fk_order_id
                where  go_tasks.job_type='Delivery' and go_order.currency ='AED' 
                and Date(go_tasks.created_date) <= '{enddate}'                
                order by go_tasks.task_id desc'''.format(enddate=enddate)     
        else:            
            sql = '''select go_tasks.booking_id as Task_ID,go_tasks.created_date as Creation_Date,  go_tasks.task_status as Order_Status,go_tasks.job_time_utc as Scheduled_Date,go_tasks.no_of_items as No_of_Items, go_tasks.name as Customer_Name, go_tasks.job_address as Address, go_tasks.cust_collected_amt as COD
            ,null as Location from go_order left join go_tasks on go_order.order_id = go_tasks.fk_order_id
            where  go_tasks.job_type='Delivery' and go_order.currency ='AED' 
            order by go_tasks.task_id desc'''        
        df=gethostname.query(sql)   
        
        rowsdata = util.getlateststatus()      
        for i, row in df.iterrows():
            if(any(item in row[0] for item in [elem[0] for elem in rowsdata])):                
                df.at[i , 'Order_Status'] = [x[1] for x in rowsdata if x[0] == row[0]][0]
                df.at[i , 'Location'] = [x[2] for x in rowsdata if x[0] == row[0]][0]

        listOfReading = [json.loads(row.to_json()) for index, row in df.iterrows()]
            
        return Response({"status": "success", "data":listOfReading}, status=status.HTTP_200_OK)

   
class SaveUpdateStatus(APIView):
    def post(self, request):
       
        reqstatus  = request.data["status"]
        reqlocation = request.data["location_id"]
        reqorg_id = request.data["org_id"]
        driver = request.data["driver"]
        taskids = request.data["taskIds"]
        
        for tid in taskids:
           od = OrderDetails()
           if OrderDetails.objects.filter(task_id=tid).exists():
            od=OrderDetails.objects.get(task_id=tid)
           od.status=reqstatus
           od.location_id=reqlocation
           od.driver_name=driver
           od.org_id=reqorg_id
           od.created_on=datetime.utcnow()  if od.created_on == None else od.created_on
           od.created_by="211228065515800633" if od.created_by == None else od.created_by
           od.updated_on=datetime.utcnow()  
           od.updated_by="211228065515800633" 
           od.task_id=tid
           od.is_active=True
           od.save()
           self.updateorderdetails(od)
        return JsonResponse(True,safe=False, status=status.HTTP_200_OK)
                      
    def updateorderdetails(self,odg):
         
        od= OrderDetailsLog()
        od.hist_task_id=util.getuniqueid()
        od.status=odg.status
        od.location_id=odg.location_id
        od.org_id=odg.org_id
        od.driver_name=odg.driver_name
        od.created_by="211228065515800633"
        od.created_on=datetime.utcnow()
        od.task_id=odg.task_id
        od.is_active=True
        od.save()
   
    
