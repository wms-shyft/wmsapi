from sshtunnel import SSHTunnelForwarder
import MySQLdb as db
import pandas as pd

host = '54.89.109.74'
localhost = 'shyft-prod-backend.cmpiztsyekx0.us-east-1.rds.amazonaws.com'
ssh_username = 'ubuntu'
ss_private_key = 'E:\Code\SSH_test\shyft-prod-key.pem'

user='shyftramprod'
password='shyftramprod#123'
database='shyftclub_master'
port='3306'

def query(q):
    with SSHTunnelForwarder(
        (host, 22),
        ssh_username=ssh_username,
        ssh_private_key=ss_private_key,
        remote_bind_address=(localhost, 3306)
    ) as server:
        conn = db.connect(host='127.0.0.1',
                               port=server.local_bind_port,
                               user=user,
                               passwd=password,
                               db=database
                               )

        return pd.read_sql_query(q, conn)
    
