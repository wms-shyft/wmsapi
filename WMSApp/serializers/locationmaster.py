from rest_framework import serializers
from WMSApp.models.locationmaster import LocationMaster 


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = LocationMaster
        fields=('__all__')