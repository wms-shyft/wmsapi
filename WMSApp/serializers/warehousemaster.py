from rest_framework import serializers
from rest_framework.compat import md_filter_add_syntax_highlight
from WMSApp.models.warehousemaster import WarehouseMaster 


class WareHouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = WarehouseMaster
        fields=('__all__')

# class WarehouseGridSerializer(serializers.Serializer):
#     warehouse_id = serializers.CharField(primary_key=True, max_length=20)
#     org_id = serializers.CharField(max_length=20, blank=True, null=True)
#     warehouse_name = serializers.CharField(max_length=100, blank=True, null=True)
#     warehouse_code = serializers.CharField(max_length=100, blank=True, null=True)
#     city_id = serializers.CharField(max_length=20, blank=True, null=True)
#     city_name=serializers.CharField(max_length=100, blank=True, null=True)
#     address = serializers.CharField(max_length=100, blank=True, null=True)
#     zipcode = serializers.CharField(max_length=10, blank=True, null=True)
#     inchareged_user_id = serializers.CharField(max_length=20, blank=True, null=True)
#     person_name = serializers.CharField(max_length=100, blank=True, null=True)
#     person_contact=serializers.CharField(max_length=20, blank=True, null=True)
#     is_default = serializers.BooleanField()
#     is_active = serializers.BooleanField()
#     created_by = serializers.CharField(max_length=20, blank=True, null=True)
#     created_on = serializers.DateTimeField()
#     updated_by = serializers.CharField(max_length=20, blank=True, null=True)
#     updated_on = serializers.DateTimeField(blank=True, null=True)

#     class Meta:
#         fields = ('__all__')