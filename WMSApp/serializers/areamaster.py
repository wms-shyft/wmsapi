from rest_framework import serializers
from WMSApp.models.areamaster import AreaMaster 


class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = AreaMaster
        fields=('__all__')