
from rest_framework import serializers
from WMSApp.models.usermaster import UserMaster 


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserMaster
        fields=('__all__')