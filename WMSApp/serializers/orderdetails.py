from rest_framework import serializers
from WMSApp.models.orderdetails import OrderDetails 


class OrderDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetails
        fields=('__all__')