from rest_framework import serializers
from WMSApp.models.citymaster import CityMaster 


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = CityMaster
        fields=('__all__')