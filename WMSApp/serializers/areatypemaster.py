from rest_framework import serializers
from WMSApp.models.areatypemaster import AreaTypeMaster 


class AreaTypeMasterSerializer(serializers.ModelSerializer):
    class Meta:
        model = AreaTypeMaster
        fields=('__all__')