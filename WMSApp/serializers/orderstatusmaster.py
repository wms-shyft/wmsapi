from rest_framework import serializers
from WMSApp.models.orderstatusmaster import  OrderStatusMaster 


class OrderStatusMasterSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderStatusMaster
        fields=('__all__')